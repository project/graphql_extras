# GraphQL Extras

Extends GraphQL v3 mostly with plugins that
facilitates the progressive decoupling (WIP).

- Current url (root field)
- Current language (root field)
- Is front page (root, Entity field)
- Extend the arbitrary Context (as Entity field)
- Path with query string (Url field)
- Entity translation from context (Entity field)
- Entity has translation (Entity field)
- File and Image relative url (File and ImageResource field, syntactic sugar)
- Image style relative url

WIP
- Url target entity (Url field)
- Url is external (Url field, for menu classes, target, ...)
- Path access for a role (Url field)

## Example queries.

### Current url / language / path with query string.

Example use case: language switcher that preserves the query string.

```graphql
{
  currentUrl {
    path: pathWithQueryString
  }
  currentLanguage {
    name
    url {
      path: pathWithQueryString
    }
    links {
      name
      url {
        path: pathWithQueryString
      }
    }
  }
}
```

### Arbitrary context extension for Entity

```graphql
{
 nodeQuery(filter: {
    conditions: [
      { field: "type", value: ["page"]}
    ]
  }) {
    entities {
      ... on NodePage {
        currentUserContext {
          uid
        }
      }
    }
  }
}
```

### Entity translation from context

```graphql
{
 nodeQuery(filter: {
    conditions: [
      { field: "type", value: ["page"]}
    ]
  }) {
    entities {
      ... on NodePage {
        entityTranslationFromContext {
          entityLabel
        }
      }
    }
  }
}
```

### Entity has translation

```graphql
{
 nodeQuery(filter: {
    conditions: [
      { field: "type", value: ["page"]}
    ]
  }) {
    entities {
      ... on NodePage {
        currentLanguage: entityHasTranslation
        otherLanguage: entityHasTranslation(language: EN)
      }
    }
  }
}
```

### Relative url's

Example use case: syntactic sugar for existing frontend or image styles.

For Files / Images, it is possible to get relative urls with `entity.uri.url`:

```graphql
{
  fileQuery {
    entities {
      ... on File {
        uri {
          url
        }
      }
    }
  }
  mediaQuery(filter: {
    conditions: [
      { field: "bundle", value: ["image"]}
    ]
  }) {
    entities {
      ... on MediaImage {
        image: fieldMediaImage {
          entity {
            uri {
              url
            }
          }
        }
      }
    }
  }
}
```

In some cases, (e.g. a frontend implementation already exists),
syntactic sugar can be used to transform absolute to relative url's.

```graphql
{
  fileQuery {
    entities {
      ... on File {
        url: relativeUrl
      }
    }
  }
  mediaQuery(filter: {
    conditions: [
      { field: "bundle", value: ["image"]}
    ]
  }) {
    entities {
      ... on MediaImage {
        image: fieldMediaImage {
          url: relativeUrl
          thumbnail: derivative(style: THUMBNAIL) {
            height
            width
            url: relativeUrl
          }
        }
      }
    }
  }
}
```
