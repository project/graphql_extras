<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Image;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve the image relative url for an image style.
 *
 * Can be used e.g. in a context of progressive decoupling.
 *
 * @GraphQLField(
 *   id = "image_style_relative_url",
 *   secure = true,
 *   name = "relativeUrl",
 *   type = "String",
 *   parents = {"ImageResource"},
 *   provider = "image",
 * )
 */
class ImageResourceRelativeUrl extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield file_url_transform_relative($value['url']);
  }

}
