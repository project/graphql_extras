<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Routing;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieves if the current page is the front page.
 *
 * @GraphQLField(
 *   id = "is_front_page",
 *   name = "isFrontPage",
 *   type = "Boolean",
 *   secure = true,
 *   parents = {
 *     "Root",
 *     "Entity",
 *   },
 * )
 */
class IsFrontPage extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Path\PathMatcherInterface definition.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->pathMatcher = $container->get('path.matcher');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $this->pathMatcher->isFrontPage();
  }

}

