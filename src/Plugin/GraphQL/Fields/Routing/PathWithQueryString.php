<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Routing;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\graphql\GraphQL\Cache\CacheableValue;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Url path with query string.
 *
 * Can be used e.g. for language switchers.
 *
 * @GraphQLField(
 *   id = "url_path_with_query_string",
 *   secure = true,
 *   name = "pathWithQueryString",
 *   description = @Translation("The processed url path with the current query string."),
 *   type = "String",
 *   response_cache_contexts = {"languages:language_url", "url.query_args"},
 *   parents = {"Url"},
 * )
 */
class PathWithQueryString extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Url) {
      $url = $value->toString(TRUE);
      $path = $url->getGeneratedUrl();
      $queryString = $this->requestStack->getCurrentRequest()->getQueryString();
      if (!empty($queryString)) {
        $path .= '?' . $queryString;
      }
      yield new CacheableValue($path, [$url]);
    }
  }

}
