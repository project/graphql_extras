<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Routing;

use Drupal\Core\Url;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve the current url.
 *
 * @GraphQLField(
 *   id = "current_url",
 *   name = "currentUrl",
 *   type = "Url",
 *   secure = true,
 *   parents = {
 *     "Root",
 *   },
 * )
 */
class CurrentUrl extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield Url::fromRoute('<current>');
  }

}
