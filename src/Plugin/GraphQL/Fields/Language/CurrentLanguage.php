<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Language;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Current language.
 *
 * @GraphQLField(
 *   id = "current_language",
 *   name = "currentLanguage",
 *   type = "Language",
 *   secure = true,
 *   parents = {
 *     "Root",
 *   },
 *   response_cache_contexts = {
 *     "user.permissions",
 *     "languages:language_interface",
 *   },
 * )
 */
class CurrentLanguage extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $this->languageManager->getCurrentLanguage();
  }

}
