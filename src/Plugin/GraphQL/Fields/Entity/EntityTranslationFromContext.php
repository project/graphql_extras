<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql_core\Plugin\GraphQL\Fields\Entity\EntityTranslation;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve an entity translation from the context.
 *
 * @GraphQLField(
 *   id = "entity_translation_from_context",
 *   name = "entityTranslationFromContext",
 *   secure = true,
 *   nullable = true,
 *   multi = false,
 *   type = "Entity",
 *   parents = {
 *     "Entity",
 *   },
 * )
 */
class EntityTranslationFromContext extends EntityTranslation {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityInterface && $value instanceof TranslatableInterface) {
      yield $this->entityRepository->getTranslationFromContext($value);
    }
  }

}
