<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Entity\Fields\Image;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve the image relative url.
 *
 * Can be used e.g. in a context of progressive decoupling.
 *
 * @GraphQLField(
 *   id = "image_relative_url",
 *   secure = true,
 *   name = "relativeUrl",
 *   type = "String",
 *   field_types = {"image"},
 *   provider = "image",
 *   deriver = "Drupal\graphql_core\Plugin\Deriver\Fields\EntityFieldPropertyDeriver",
 * )
 */
class ImageRelativeUrl extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof ImageItem && $value->entity && $value->entity->access('view')) {
      $absolute_url = file_create_url($value->entity->getFileUri());
      yield file_url_transform_relative($absolute_url);
    }
  }

}
