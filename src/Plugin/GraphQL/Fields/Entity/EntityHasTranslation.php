<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve if an entity has a translation for a language.
 *
 * If no language is specified, uses the current language.
 * Useful to know if there is a translation fallback.
 *
 * @GraphQLField(
 *   id = "entity_has_translation",
 *   name = "entityHasTranslation",
 *   secure = true,
 *   nullable = true,
 *   multi = false,
 *   type = "Boolean",
 *   arguments = {
 *     "language" = "LanguageId"
 *   },
 *   parents = {
 *     "Entity",
 *   },
 * )
 */
class EntityHasTranslation extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $result = FALSE;
    if ($value instanceof EntityInterface && $value instanceof TranslatableInterface && $value->isTranslatable()) {
      $languageCode = isset($args['language']) ? $args['language'] : $this->languageManager->getCurrentLanguage()->getId();
      yield $value->hasTranslation($languageCode);
    }
    yield $result;
  }

}
