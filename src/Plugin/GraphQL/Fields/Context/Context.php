<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\Context;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Url;
use Drupal\graphql\GraphQL\Buffers\SubRequestBuffer;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Request arbitrary drupal context objects with GraphQL.
 *
 * @GraphQLField(
 *   id = "context",
 *   secure = true,
 *   parents = {"Root", "InternalUrl", "Entity"},
 *   deriver = "Drupal\graphql_core\Plugin\Deriver\Fields\ContextDeriver",
 *   weight = "1",
 * )
 */
class Context extends \Drupal\graphql_core\Plugin\GraphQL\Fields\Context\Context {
  // Adds the Entity parent.
}
