<?php

namespace Drupal\graphql_extras\Plugin\GraphQL\Fields\File;

use Drupal\file\FileInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Retrieve the file relative url.
 *
 * @GraphQLField(
 *   id = "file_relative_url",
 *   secure = true,
 *   name = "relativeUrl",
 *   type = "String",
 *   parents = {"File"}
 * )
 */
class FileUrl extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof FileInterface) {
      $absolute_url = file_create_url($value->getFileUri());
      yield file_url_transform_relative($absolute_url);
    }
  }

}
